Module 104 Exercice du 2021.04.21
---


# Ajoutez des joueurs panini a ta collection :
##### BUT : Ajoutez son nom, prenom, mail.. , ajoutez des joueurs et les associer a votre identiter.
* Démarrer le serveur MySql (uwamp ou xamp ou mamp, etc)
* Dans PyCharm, importer la BD grâce à un "run" du fichier "zzzdemos/1_ImportationDumpSql.py".
  * En cas d'erreurs : ouvrir le fichier ".env" à la racine du projet, contrôler les indications de connexion pour la bd.
* Puis dans le répertoire racine du projet, ouvrir le fichier "1_run_server_flask.py" et faire un "run".
* Choisir le menu "Joueures".
* Utiliser la fonction "AJOUTER"
* Si le joueurs change de club vous pouvez le changer a l'aide du bouton "EDIT"
* Si vous ne possodez plus ce joueur utilisez le bouton "DELETE"
* Ajoutez ensuite votre identiter, nom, prenom..
* Aller dans la table intermédiaire "t_joueur_personne" qui se situe dans la barre de menu en haut de votre page internet.
* Ajoutez ensuite a chaque joueur que vous possedez votre nom de famille une fois le bouton modifier appuyer.
* Appuyez une seconde fois sur modifier pour valider vos changement et le tour est jouer ;)
* Répétez cet action pour le nombre de joueur que vous avez et que vous voulez associer a votre nom.


