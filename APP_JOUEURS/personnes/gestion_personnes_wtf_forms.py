"""
    Fichier : gestion_personnes_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterPersonnes(FlaskForm):
    """
        Dans le formulaire "personnes_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_personne_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_personne_wtf = StringField("Nom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_personne_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])

    prenom_personne_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    prenom_personne_wtf = StringField("Prénom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                         Regexp(prenom_personne_regexp,
                                                                                message="Pas de chiffres, de caractères "
                                                                                        "spéciaux, "
                                                                                        "d'espace à double, de double "
                                                                                        "apostrophe, de double trait union")
                                                                         ])

    adress_mail_personne_regexp = ""
    adress_mail_personne_wtf = StringField("Adresse mail ",
                                      validators=[Length(min=5, max=500 , message="min 2 max 20"),
                                                  Regexp(adress_mail_personne_regexp,
                                                         message="saisissez une adres mail valide")
                                                  ])

    date_naissance_personne_regexp = ""
    date_naissance_personne_wtf = StringField("Date de naissance ",
                                           validators=[Length(min=0, max=200, message="min 2 max 20"),
                                                       Regexp(date_naissance_personne_regexp)
                                                       ])
    submit = SubmitField("Enregistrer personne")


class FormWTFUpdatePersonne(FlaskForm):
    """
        Dans le formulaire "personne_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Adress_Mail_personne_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    Adress_Mail_personne_update_wtf = StringField("Modifier le mail ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(Adress_Mail_personne_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    submit = SubmitField("Update personne")


class FormWTFDeletePersonne(FlaskForm):
    """
        Dans le formulaire "personne_delete_wtf.html"

        nom_personne_delete_wtf : Champ qui reçoit la valeur du personne, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "personne".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_personne".
    """
    nom_personne_delete_wtf = StringField("Effacer ce personne")
    submit_btn_del = SubmitField("Effacer personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
