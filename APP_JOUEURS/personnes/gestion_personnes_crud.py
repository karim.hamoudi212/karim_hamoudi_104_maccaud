"""
    Fichier : gestion_personnes_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les personnes.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_JOUEURS import obj_mon_application
from APP_JOUEURS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_JOUEURS.erreurs.exceptions import *
from APP_JOUEURS.erreurs.msg_erreurs import *
from APP_JOUEURS.personnes.gestion_personnes_wtf_forms import FormWTFAjouterPersonnes
from APP_JOUEURS.personnes.gestion_personnes_wtf_forms import FormWTFDeletePersonne
from APP_JOUEURS.personnes.gestion_personnes_wtf_forms import FormWTFUpdatePersonne

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /personnes_afficher
    
    Test : ex : http://127.0.0.1:5005/personnes_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_personne_sel = 0 >> tous les personnes.
                id_personne_sel = "n" affiche le personne dont l'id est "n"
"""


@obj_mon_application.route("/personnes_afficher/<string:order_by>/<int:id_personne_sel>", methods=['GET', 'POST'])
def personnes_afficher(order_by, id_personne_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion personnes ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionPersonnes {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_personne_sel == 0:
                    strsql_personnes_afficher = """SELECT id_personne, nom_personne, prenom_personne, Adress_Mail_personne, Date_Naissance_personne  FROM t_personne ORDER BY id_personne ASC"""
                    mc_afficher.execute(strsql_personnes_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_personne"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                    valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_sel}
                    strsql_personnes_afficher = """SELECT id_personne, nom_personne, prenom_personne, Adress_Mail_personne, Date_Naissance_personne FROM t_personne WHERE id_personne = %(value_id_personne_selected)s"""

                    mc_afficher.execute(strsql_personnes_afficher, valeur_id_personne_selected_dictionnaire)
                else:
                    strsql_personnes_afficher = """SELECT id_personne, nom_personne FROM t_personne ORDER BY id_personne DESC"""

                    mc_afficher.execute(strsql_personnes_afficher)

                data_personnes = mc_afficher.fetchall()

                print("data_personnes ", data_personnes, " Type : ", type(data_personnes))

                # Différencier les messages si la table est vide.
                if not data_personnes and id_personne_sel == 0:
                    flash("""La table "t_personne" est vide. !!""", "warning")
                elif not data_personnes and id_personne_sel > 0:
                    # Si l'utilisateur change l'id_personne dans l'URL et que le personne n'existe pas,
                    flash(f"Le personne demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_personne" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données personnes affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. personnes_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} personnes_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("personnes/personnes_afficher.html", data=data_personnes)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /personnes_ajouter
    
    Test : ex : http://127.0.0.1:5005/personnes_ajouter
    
    Paramètres : sans
    
    But : Ajouter un personne pour un joueurs
    
    Remarque :  Dans le champ "name_personne_html" du formulaire "personnes/personnes_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/personnes_ajouter", methods=['GET', 'POST'])
def personnes_ajouter_wtf():
    form = FormWTFAjouterPersonnes()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion personnes ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionPersonnes {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_personne_wtf = form.nom_personne_wtf.data
                name_personne = name_personne_wtf.lower()
                prenom_personne_wtf = form.prenom_personne_wtf.data
                prenom_personne = prenom_personne_wtf.lower()
                adress_mail_personne_wtf = form.adress_mail_personne_wtf.data
                adress_mail_personne = adress_mail_personne_wtf.lower()
                date_naissance_personne_wtf = form.date_naissance_personne_wtf.data
                date_naissance_personne = date_naissance_personne_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_Nom_personne": name_personne,
                                                  "value_prenom_personne": prenom_personne,
                                                  "value_Adress_Mail_personne": adress_mail_personne,
                                                  "value_Date_Naissance_personne": date_naissance_personne}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_personne = """INSERT INTO t_personne (id_personne,Nom_personne, prenom_personne,Adress_Mail_personne, Date_Naissance_personne) VALUES (NULL,%(value_Nom_personne)s, %(value_prenom_personne)s, %(value_Adress_Mail_personne)s, %(value_Date_Naissance_personne)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_personne, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('personnes_afficher', order_by='ASC', id_personne_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_personne_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_personne_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_personn_crud:
            code, msg = erreur_gest_personn_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion personnes CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_personn_crud.args[0]} , "
                  f"{erreur_gest_personn_crud}", "danger")

    return render_template("personnes/personnes_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /personne_update
    
    Test : ex cliquer sur le menu "personnes" puis cliquer sur le bouton "EDIT" d'un "personne"
    
    Paramètres : sans
    
    But : Editer(update) un personne qui a été sélectionné dans le formulaire "personnes_afficher.html"
    
    Remarque :  Dans le champ "nom_personne_update_wtf" du formulaire "personnes/personne_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/personne_update", methods=['GET', 'POST'])
def personne_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_personne"
    id_personne_update = request.values['id_personne_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatePersonne()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "personne_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            Adress_Mail_personne_update = form_update.Adress_Mail_personne_update_wtf.data
            Adress_Mail_personne_update = Adress_Mail_personne_update.lower()

            valeur_update_dictionnaire = {"value_id_personne": id_personne_update, "value_Adress_Mail_personne_personne": Adress_Mail_personne_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulepersonne = """UPDATE t_personne SET Adress_Mail_personne = %(value_Adress_Mail_personne)s WHERE id_personne = %(value_id_personne)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulepersonne, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_personne_update"
            return redirect(url_for('personnes_afficher', order_by="ASC", id_personne_sel=id_personne_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_personne" et "nom_personne" de la "t_personne"
            str_sql_id_personne = "SELECT id_personne, Adress_Mail_personne FROM t_personne WHERE id_personne = %(value_id_personne)s"
            valeur_select_dictionnaire = {"value_id_personne": id_personne_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_personne, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom personne" pour l'UPDATE
            data_Adress_Mail_personne = mybd_curseur.fetchone()
            print("data_Adress_Mail_personne ", data_Adress_Mail_personne, " type ", type(data_Adress_Mail_personne), " personne ",
                  data_Adress_Mail_personne["Adress_Mail_personne"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "personne_update_wtf.html"
            form_update.Adress_Mail_personne_update_wtf.data = data_Adress_Mail_personne["Adress_Mail_personne"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans personne_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans personne_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_personn_crud:
        code, msg = erreur_gest_personn_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_personn_crud} ", "danger")
        flash(f"Erreur dans personne_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_personn_crud.args[0]} , "
              f"{erreur_gest_personn_crud}", "danger")
        flash(f"__KeyError dans personne_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("personnes/personne_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /personne_delete
    
    Test : ex. cliquer sur le menu "personnes" puis cliquer sur le bouton "DELETE" d'un "personne"
    
    Paramètres : sans
    
    But : Effacer(delete) un personne qui a été sélectionné dans le formulaire "personnes_afficher.html"
    
    Remarque :  Dans le champ "nom_personne_delete_wtf" du formulaire "personnes/personne_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/personne_delete", methods=['GET', 'POST'])
def personne_delete_wtf():
    data_joueures_attribue_personne_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_personne"
    id_personne_delete = request.values['id_personne_btn_delete_html']

    # Objet formulaire pour effacer le personne sélectionné.
    form_delete = FormWTFDeletePersonne()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("personnes_afficher", order_by="ASC", id_personne_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "personnes/personne_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_joueures_attribue_personne_delete = session['data_joueures_attribue_personne_delete']
                print("data_joueures_attribue_personne_delete ", data_joueures_attribue_personne_delete)

                flash(f"Effacer le personne de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer personne" qui va irrémédiablement EFFACER le personne
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_personne": id_personne_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_joueures_personne = """DELETE FROM t_joueur_personne WHERE fk_Id_personne  = %(value_id_personne)s"""
                str_sql_delete_idpersonne = """DELETE FROM t_personne WHERE id_personne = %(value_id_personne)s"""
                # Manière brutale d'effacer d'abord la "fk_Id_personne ", même si elle n'existe pas dans la "t_joueur_personne"
                # Ensuite on peut effacer le personne vu qu'il n'est plus "lié" (INNODB) dans la "t_joueur_personne"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_joueures_personne, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idpersonne, valeur_delete_dictionnaire)

                flash(f"Personne définitivement effacé !!", "success")
                print(f"Personne définitivement effacé !!")

                # afficher les données
                return redirect(url_for('personnes_afficher', order_by="ASC", id_personne_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_personne": id_personne_delete}
            print(id_personne_delete, type(id_personne_delete))

            # Requête qui affiche tous les joueures_personnes qui ont le personne que l'utilisateur veut effacer
            str_sql_personnes_joueures_delete = """SELECT Id_Avoir_Joueur, Nom_Joueur, id_personne, nom_personne FROM t_joueur_personne 
                                            INNER JOIN t_carte_joueur ON t_joueur_personne.fk_Id_Joueur = t_carte_joueur.Id_Joueur
                                            INNER JOIN t_personne ON t_joueur_personne.fk_Id_personne  = t_personne.id_personne
                                            WHERE fk_Id_personne  = %(value_id_personne)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_personnes_joueures_delete, valeur_select_dictionnaire)
            data_joueures_attribue_personne_delete = mybd_curseur.fetchall()
            print("data_joueures_attribue_personne_delete...", data_joueures_attribue_personne_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "personnes/personne_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_joueures_attribue_personne_delete'] = data_joueures_attribue_personne_delete

            # Opération sur la BD pour récupérer "id_personne" et "nom_personne" de la "t_personne"
            str_sql_id_personne = "SELECT id_personne, nom_personne FROM t_personne WHERE id_personne = %(value_id_personne)s"

            mybd_curseur.execute(str_sql_id_personne, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom personne" pour l'action DELETE
            data_nom_personne = mybd_curseur.fetchone()
            print("data_nom_personne ", data_nom_personne, " type ", type(data_nom_personne), " personne ",
                  data_nom_personne["nom_personne"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "personne_delete_wtf.html"
            form_delete.nom_personne_delete_wtf.data = data_nom_personne["nom_personne"]

            # Le bouton pour l'action "DELETE" dans le form. "personne_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans personne_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans personne_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_personn_crud:
        code, msg = erreur_gest_personn_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_personn_crud} ", "danger")

        flash(f"Erreur dans personne_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_personn_crud.args[0]} , "
              f"{erreur_gest_personn_crud}", "danger")

        flash(f"__KeyError dans personne_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("personnes/personne_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_joueures_associes=data_joueures_attribue_personne_delete)
