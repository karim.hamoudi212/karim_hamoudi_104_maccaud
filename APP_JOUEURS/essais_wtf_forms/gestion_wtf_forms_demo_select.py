"""
    Fichier : gestion_joueures_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les joueures.
"""
import sys

import pymysql
from flask import flash
from flask import render_template
from flask import request
from flask import session

from APP_JOUEURS import obj_mon_application
from APP_JOUEURS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_JOUEURS.erreurs.msg_erreurs import *
from APP_JOUEURS.essais_wtf_forms.wtf_forms_demo_select import DemoFormSelectWTF

"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /joueure_delete
    
    Test : ex. cliquer sur le menu "joueures" puis cliquer sur le bouton "DELETE" d'un "joueure"
    
    Paramètres : sans
    
    But : Effacer(delete) un joueure qui a été sélectionné dans le formulaire "joueures_afficher.html"
    
    Remarque :  Dans le champ "nom_joueure_delete_wtf" du formulaire "joueures/joueure_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/demo_select_wtf", methods=['GET', 'POST'])
def demo_select_wtf():
    joueure_selectionne = None
    # Objet formulaire pour montrer une liste déroulante basé sur la table "t_carte_joueur"
    form_demo = DemoFormSelectWTF()
    try:
        if request.method == "POST" and form_demo.submit_btn_ok_dplist_carte_joueur.data:

            if form_demo.submit_btn_ok_dplist_carte_joueur.data:
                print("Joueure sélectionné : ",
                      form_demo.joueures_dropdown_wtf.data)
                joueure_selectionne = form_demo.joueures_dropdown_wtf.data
                form_demo.joueures_dropdown_wtf.choices = session['joueure_val_list_dropdown']

        if request.method == "GET":
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_joueures_afficher = """SELECT id_Joueur, Prenom_Joueur FROM t_carte_joueur ORDER BY id_Joueur ASC"""
                mc_afficher.execute(strsql_joueures_afficher)

            data_joueures = mc_afficher.fetchall()
            print("demo_select_wtf data_joueures ", data_joueures, " Type : ", type(data_joueures))

            """
                Préparer les valeurs pour la liste déroulante de l'objet "form_demo"
                la liste déroulante est définie dans le "wtf_forms_demo_select.py" 
                le formulaire qui utilise la liste déroulante "zzz_essais_om_104/demo_form_select_wtf.html"
            """
            joueure_val_list_dropdown = []
            for i in data_joueures:
                joueure_val_list_dropdown.append(i['Prenom_Joueur'])

            # Aussi possible d'avoir un id numérique et un texte en correspondance
            # joueure_val_list_dropdown = [(i["id_Joueur"], i["Prenom_Joueur"]) for i in data_joueures]

            print("joueure_val_list_dropdown ", joueure_val_list_dropdown)

            form_demo.joueures_dropdown_wtf.choices = joueure_val_list_dropdown
            session['joueure_val_list_dropdown'] = joueure_val_list_dropdown
            # Ceci est simplement une petite démo. on fixe la valeur PRESELECTIONNEE de la liste
            form_demo.joueures_dropdown_wtf.data = "philosophique"
            joueure_selectionne = form_demo.joueures_dropdown_wtf.data
            print("joueure choisi dans la liste :", joueure_selectionne)
            session['joueure_selectionne_get'] = joueure_selectionne

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_joueur_crud:
        code, msg = erreur_gest_joueur_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_joueur_crud} ", "danger")

        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} "
              f"{erreur_gest_joueur_crud.args[0]} , "
              f"{erreur_gest_joueur_crud}", "danger")

        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("zzz_essais_om_104/demo_form_select_wtf.html",
                           form=form_demo,
                           joueure_selectionne=joueure_selectionne)
