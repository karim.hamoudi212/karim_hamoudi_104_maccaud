-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 26 mai 2021 à 11:32
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21
--
-- Base de données : `hamoudi_karim_info1a_panini_104_2021`
--

-- --------------------------------------------------------
DROP DATABASE IF EXISTS Hamoudi_Karim_INFO1A_Panini_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS Hamoudi_Karim_INFO1A_Panini_104_2021;

-- Utilisation de cette base de donnée

USE Hamoudi_Karim_INFO1A_Panini_104_2021;
--
-- Structure de la table `t_carte_joueur`
-- 4 e

CREATE TABLE `t_carte_joueur` (
  `Id_Joueur` int(11) NOT NULL,
  `Prenom_Joueur` varchar(255) DEFAULT NULL,
  `Nom_Joueur`varchar(255) DEFAULT NULL,
  `Nationnaliter_Joueur` varchar(255) DEFAULT NULL,
  `Club_Joueur` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_carte_joueur`
--

INSERT INTO `t_carte_joueur` (`Id_Joueur`, `Prenom_Joueur`, `Nom_Joueur`, `Nationnaliter_Joueur`, `Club_Joueur`) VALUES
(1, 'Cristiano', 'Ronaldo', 'Portugais', 'Juventus'),
(2, 'dybala', 'paolo', 'argentin', 'juventus'),
(3, 'Lionel', 'Messi', 'Argentin', 'Barcelone'),
(4, 'Paul', 'Pogba', 'Francais', 'Manchester United'),
(5, 'Kyllian', 'Mbapper', 'Francais', 'Paris Saint Germain'),
(6, 'JR', 'Neymar', 'Bresilien', 'Paris Saint Germain'),
(7, 'Zinedine', 'Zidane', 'Francais', 'Equipe de france'),
(8, 'Antoine', 'Griezmann', 'Francais', 'Barcelone'),
(9, 'Kevin', 'De Bruyne', 'Belge', 'Manchester City'),
(10, 'Olivier', 'Giroud', 'Francais', 'Chelsea');

-- --------------------------------------------------------

--
-- Structure de la table `t_joueur_personne`
--

CREATE TABLE `t_joueur_personne` (
  `Id_Avoir_Joueur` int(11) NOT NULL ,
  `fk_Id_Joueur` int(11) NOT NULL,
  `fk_Id_personne` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE`t_personne` (
  `Id_personne` int(11) NOT NULL,
  `Nom_personne` varchar(255) DEFAULT NULL,
  `prenom_personne`varchar(255) DEFAULT NULL,
  `Adress_Mail_personne` varchar(255) DEFAULT NULL,
  `Date_Naissance_personne` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_personne`
--

INSERT INTO `t_personne` (`Id_personne`, `Nom_personne`, `prenom_personne`, `Adress_Mail_personne`, `Date_Naissance_personne`) VALUES
(1, 'Hamoudi', 'Karim', 'kbadhjk@gmail.com', '2026-12-20'),
(2, 'acevedo', 'jesus', 'jesus@gmail.com', '2021-06-11'),
(3, 'Maccaud', 'Olivier', 'olivier.maccaud@gmail.com', '2020-08-04'),
(4, 'Rouiller', 'Julien', 'julien.roullier@eduvaud.ch', '2021-06-08');


ALTER TABLE `t_carte_Joueur`
  ADD PRIMARY KEY (`id_Joueur`),
--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`id_personne`);

--
-- Index pour la table `t_joueur_personne`
--
ALTER TABLE `t_joueur_personne`
  ADD PRIMARY KEY (`Id_Avoir_Joueur`),
  ADD KEY `fk_Id_Joueur` (`fk_Id_Joueur`),
  ADD KEY `fk_Id_personne` (`fk_Id_personne`);

--
-- Contraintes pour les tables déchargées
--
ALTER TABLE `t_carte_Joueur`
  MODIFY `id_Joueur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `t_joueur_personne`
--
ALTER TABLE `t_joueur_personne`
  MODIFY `id_Avoir_Joueur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;
--
-- Contraintes pour la table `t_joueur_personne`
--
ALTER TABLE `t_joueur_personne`
  ADD CONSTRAINT `t_personne_joueure_ibfk_1` FOREIGN KEY (`fk_Id_Joueur`) REFERENCES `t_carte_joueur` (`Id_Joueur`),
  ADD CONSTRAINT `t_personne_joueure_ibfk_2` FOREIGN KEY (`fk_Id_personne`) REFERENCES `t_personne` (`Id_personne`);

