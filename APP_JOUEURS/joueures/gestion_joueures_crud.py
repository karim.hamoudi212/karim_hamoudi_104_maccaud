"""
    Fichier : gestion_joueures_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les joueures.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_JOUEURS import obj_mon_application
from APP_JOUEURS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_JOUEURS.erreurs.exceptions import *
from APP_JOUEURS.erreurs.msg_erreurs import *
from APP_JOUEURS.joueures.gestion_joueures_wtf_forms import FormWTFAjouterJoueures
from APP_JOUEURS.joueures.gestion_joueures_wtf_forms import FormWTFDeleteJoueure
from APP_JOUEURS.joueures.gestion_joueures_wtf_forms import FormWTFUpdateJoueure

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /joueures_afficher
    
    Test : ex : http://127.0.0.1:5005/joueures_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_Joueur_sel = 0 >> tous les joueures.
                id_Joueur_sel = "n" affiche le joueure dont l'id est "n"
"""


@obj_mon_application.route("/joueures_afficher/<string:order_by>/<int:id_Joueur_sel>", methods=['GET', 'POST'])
def joueures_afficher(order_by, id_Joueur_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion joueures ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionJoueures {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_Joueur_sel == 0:
                    strsql_joueures_afficher = """SELECT id_Joueur, Prenom_Joueur, Nom_Joueur, Nationnaliter_Joueur, Club_Joueur FROM t_carte_joueur ORDER BY id_Joueur ASC"""
                    mc_afficher.execute(strsql_joueures_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_carte_joueur"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du joueure sélectionné avec un nom de variable
                    valeur_id_Joueur_selected_dictionnaire = {"value_id_Joueur_selected": id_Joueur_sel}
                    strsql_joueures_afficher = """SELECT id_Joueur, Prenom_Joueur, Nom_Joueur, Nationnaliter_Joueur, Club_Joueur FROM t_carte_joueur WHERE id_Joueur = %(value_id_Joueur_selected)s"""

                    mc_afficher.execute(strsql_joueures_afficher, valeur_id_Joueur_selected_dictionnaire)
                else:
                    strsql_joueures_afficher = """SELECT id_Joueur, Prenom_Joueur FROM t_carte_joueur ORDER BY id_Joueur DESC"""

                    mc_afficher.execute(strsql_joueures_afficher)

                data_joueures = mc_afficher.fetchall()

                print("data_joueures ", data_joueures, " Type : ", type(data_joueures))

                # Différencier les messages si la table est vide.
                if not data_joueures and id_Joueur_sel == 0:
                    flash("""La table "t_carte_joueur" est vide. !!""", "warning")
                elif not data_joueures and id_Joueur_sel > 0:
                    # Si l'utilisateur change l'id_Joueur dans l'URL et que le joueure n'existe pas,
                    flash(f"Le joueure demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_carte_joueur" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données joueures affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. joueures_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} joueures_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("joueures/joueures_afficher.html", data=data_joueures)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /joueures_ajouter
    
    Test : ex : http://127.0.0.1:5005/joueures_ajouter
    
    Paramètres : sans
    
    But : Ajouter un joueure pour un personne
    
    Remarque :  Dans le champ "name_joueure_html" du formulaire "joueures/joueures_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/joueures_ajouter", methods=['GET', 'POST'])
def joueures_ajouter_wtf():
    form = FormWTFAjouterJoueures()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion joueures ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionJoueures {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_joueure_wtf = form.nom_joueure_wtf.data
                name_joueure = name_joueure_wtf.lower()
                prenom_joueure_wtf = form.prenom_joueure_wtf.data
                prenom_joueure = prenom_joueure_wtf.lower()
                nationnalite_joueure_wtf = form.nationnalite_joueure_wtf.data
                nationnalite_joueure = nationnalite_joueure_wtf.lower()
                club_joueure_wtf = form.club_joueure_wtf.data
                club_joueure = club_joueure_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_Prenom_Joueur": name_joueure,
                                                  "value_Nom_Joueur" : prenom_joueure,
                                                  "value_Nationnaliter_Joueur": nationnalite_joueure,
                                                  "value_Club_Joueur": club_joueure}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_carte_joueur = """INSERT INTO t_carte_joueur (id_Joueur,Prenom_Joueur, Nom_Joueur, Nationnaliter_Joueur, Club_Joueur) VALUES (NULL,%(value_Prenom_Joueur)s, %(value_Nom_Joueur)s, %(value_Nationnaliter_Joueur)s, %(value_Club_Joueur)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_carte_joueur, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('joueures_afficher', order_by='ASC', id_Joueur_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_joueure_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_joueure_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_joueur_crud:
            code, msg = erreur_gest_joueur_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion joueures CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_joueur_crud.args[0]} , "
                  f"{erreur_gest_joueur_crud}", "danger")

    return render_template("joueures/joueures_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /joueure_update
    
    Test : ex cliquer sur le menu "joueures" puis cliquer sur le bouton "EDIT" d'un "joueure"
    
    Paramètres : sans
    
    But : Editer(update) un joueure qui a été sélectionné dans le formulaire "joueures_afficher.html"
    
    Remarque :  Dans le champ "nom_joueure_update_wtf" du formulaire "joueures/joueure_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/joueure_update", methods=['GET', 'POST'])
def joueure_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_Joueur"
    id_Joueur_update = request.values['id_Joueur_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateJoueure()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "joueure_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            Club_Joueur_update = form_update.Club_Joueur_update_wtf.data
            Club_Joueur_update = Club_Joueur_update.lower()

            valeur_update_dictionnaire = {"value_id_Joueur": id_Joueur_update, "value_Club_Joueur": Club_Joueur_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_Club_Joueur = """UPDATE t_carte_joueur SET Club_Joueur = %(value_Club_Joueur)s WHERE id_Joueur = %(value_id_Joueur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_Club_Joueur, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_Joueur_update"
            return redirect(url_for('joueures_afficher', order_by="ASC", id_Joueur_sel=id_Joueur_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_Joueur" et "Club_Joueur" de la "t_carte_joueur"
            str_sql_id_Joueur = "SELECT id_Joueur, Club_Joueur FROM t_carte_joueur WHERE id_Joueur = %(value_id_Joueur)s"
            valeur_select_dictionnaire = {"value_id_Joueur": id_Joueur_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_Joueur, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom joueure" pour l'UPDATE
            data_Club_Joueur = mybd_curseur.fetchone()
            print("data_Club_Joueur ", data_Club_Joueur, " type ", type(data_Club_Joueur), " joueure ",
                  data_Club_Joueur["Club_Joueur"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "joueure_update_wtf.html"
            form_update.Club_Joueur_update_wtf.data = data_Club_Joueur["Club_Joueur"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans joueure_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans joueure_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_joueur_crud:
        code, msg = erreur_gest_joueur_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_joueur_crud} ", "danger")
        flash(f"Erreur dans joueure_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_joueur_crud.args[0]} , "
              f"{erreur_gest_joueur_crud}", "danger")
        flash(f"__KeyError dans joueure_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("joueures/joueure_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /joueure_delete
    
    Test : ex. cliquer sur le menu "joueures" puis cliquer sur le bouton "DELETE" d'un "joueure"
    
    Paramètres : sans
    
    But : Effacer(delete) un joueure qui a été sélectionné dans le formulaire "joueures_afficher.html"
    
    Remarque :  Dans le champ "nom_joueure_delete_wtf" du formulaire "joueures/joueure_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/joueure_delete", methods=['GET', 'POST'])
def joueure_delete_wtf():
    data_personnes_attribue_joueure_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_Joueur"
    id_Joueur_delete = request.values['id_Joueur_btn_delete_html']

    # Objet formulaire pour effacer le joueure sélectionné.
    form_delete = FormWTFDeleteJoueure()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("joueures_afficher", order_by="ASC", id_Joueur_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "joueures/joueure_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_personnes_attribue_joueure_delete = session['data_personnes_attribue_joueure_delete']
                print("data_personnes_attribue_joueure_delete ", data_personnes_attribue_joueure_delete)

                flash(f"Effacer le joueure de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer joueure" qui va irrémédiablement EFFACER le joueure
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_Joueur": id_Joueur_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_personnes_joueure = """DELETE FROM t_joueur_personne WHERE fk_Id_Joueur  = %(value_id_Joueur)s"""
                str_sql_delete_idjoueure = """DELETE FROM t_carte_joueur WHERE id_Joueur = %(value_id_Joueur)s"""
                # Manière brutale d'effacer d'abord la "fk_Id_Joueur ", même si elle n'existe pas dans la "t_joueur_personne"
                # Ensuite on peut effacer le joueure vu qu'il n'est plus "lié" (INNODB) dans la "t_joueur_personne"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_personnes_joueure, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idjoueure, valeur_delete_dictionnaire)

                flash(f"Joueure définitivement effacé !!", "success")
                print(f"Joueure définitivement effacé !!")

                # afficher les données
                return redirect(url_for('joueures_afficher', order_by="ASC", id_Joueur_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_Joueur": id_Joueur_delete}
            print(id_Joueur_delete, type(id_Joueur_delete))

            # Requête qui affiche tous les personnes qui ont le joueure que l'utilisateur veut effacer
            str_sql_joueures_personnes_delete = """SELECT Id_Avoir_Joueur, Nom_personne, id_Joueur, Prenom_Joueur FROM t_joueur_personne 
                                            INNER JOIN t_personne ON t_joueur_personne.fk_Id_personne  = t_personne.Id_personne
                                            INNER JOIN t_carte_joueur ON t_joueur_personne.fk_Id_Joueur  = t_carte_joueur.id_Joueur
                                            WHERE fk_Id_Joueur  = %(value_id_Joueur)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_joueures_personnes_delete, valeur_select_dictionnaire)
            data_personnes_attribue_joueure_delete = mybd_curseur.fetchall()
            print("data_personnes_attribue_joueure_delete...", data_personnes_attribue_joueure_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "joueures/joueure_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_personnes_attribue_joueure_delete'] = data_personnes_attribue_joueure_delete

            # Opération sur la BD pour récupérer "id_Joueur" et "Prenom_Joueur" de la "t_carte_joueur"
            str_sql_id_Joueur = "SELECT id_Joueur, Prenom_Joueur FROM t_carte_joueur WHERE id_Joueur = %(value_id_Joueur)s"

            mybd_curseur.execute(str_sql_id_Joueur, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom joueure" pour l'action DELETE
            data_nom_joueure = mybd_curseur.fetchone()
            print("data_nom_joueure ", data_nom_joueure, " type ", type(data_nom_joueure), " joueure ",
                  data_nom_joueure["Prenom_Joueur"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "joueure_delete_wtf.html"
            form_delete.nom_joueure_delete_wtf.data = data_nom_joueure["Prenom_Joueur"]

            # Le bouton pour l'action "DELETE" dans le form. "joueure_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans joueure_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans joueure_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_joueur_crud:
        code, msg = erreur_gest_joueur_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_joueur_crud} ", "danger")

        flash(f"Erreur dans joueure_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_joueur_crud.args[0]} , "
              f"{erreur_gest_joueur_crud}", "danger")

        flash(f"__KeyError dans joueure_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("joueures/joueure_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_personnes_associes=data_personnes_attribue_joueure_delete)
