"""
    Fichier : gestion_joueures_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterJoueures(FlaskForm):
    """
        Dans le formulaire "joueures_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_joueure_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_joueure_wtf = StringField("Prénom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_joueure_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])

    prenom_joueure_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    prenom_joueure_wtf = StringField("Nom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                       Regexp(nom_joueure_regexp,
                                                                              message="Pas de chiffres, de caractères "
                                                                                      "spéciaux, "
                                                                                      "d'espace à double, de double "
                                                                                      "apostrophe, de double trait union")
                                                                       ])
    nationnalite_joueure_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nationnalite_joueure_wtf = StringField("Nationnalite ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                            Regexp(nationnalite_joueure_regexp,
                                                                   message="Pas de chiffres, de caractères "
                                                                           "spéciaux, "
                                                                           "d'espace à double, de double "
                                                                           "apostrophe, de double trait union")
                                                            ])
    club_joueure_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    club_joueure_wtf = StringField("Club ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                        Regexp(club_joueure_regexp,
                                                                               message="Pas de chiffres, de caractères "
                                                                                       "spéciaux, "
                                                                                       "d'espace à double, de double "
                                                                                       "apostrophe, de double trait union")
                                                                        ])
    submit = SubmitField("Enregistrer joueure")


class FormWTFUpdateJoueure(FlaskForm):
    """
        Dans le formulaire "joueure_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Club_Joueur_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    Club_Joueur_update_wtf = StringField("Club ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(Club_Joueur_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])

    submit = SubmitField("Update joueure")


class FormWTFDeleteJoueure(FlaskForm):
    """
        Dans le formulaire "joueure_delete_wtf.html"

        nom_joueure_delete_wtf : Champ qui reçoit la valeur du joueure, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "joueure".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_carte_joueur".
    """
    nom_joueure_delete_wtf = StringField("Effacer ce joueure")
    submit_btn_del = SubmitField("Effacer joueure")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
