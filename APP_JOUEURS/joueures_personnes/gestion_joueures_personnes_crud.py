"""
    Fichier : gestion_joueures_personnes_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les joueures et les personnes.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_JOUEURS import obj_mon_application
from APP_JOUEURS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_JOUEURS.erreurs.exceptions import *
from APP_JOUEURS.erreurs.msg_erreurs import *

"""
    Nom : joueures_personnes_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /joueures_personnes_afficher
    
    But : Afficher les joueures avec les personnes associés pour chaque joueure.
    
    Paramètres : id_personne_sel = 0 >> tous les joueures.
                 id_personne_sel = "n" affiche le joueure dont l'id est "n"
                 
"""


@obj_mon_application.route("/joueures_personnes_afficher/<int:id_joueure_sel>", methods=['GET', 'POST'])
def joueures_personnes_afficher(id_joueure_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_joueures_personnes_afficher:
                code, msg = Exception_init_joueures_personnes_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_joueures_personnes_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_joueures_personnes_afficher.args[0]} , "
                      f"{Exception_init_joueures_personnes_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_personnes_joueures_afficher_data = """SELECT Id_Joueur, Nom_Joueur, Prenom_Joueur, Club_Joueur, Nationnaliter_Joueur, 
                                                            GROUP_CONCAT(prenom_personne) as Id_Avoir_Joueur FROM t_joueur_personne
                                                            RIGHT JOIN t_carte_joueur ON t_carte_joueur.Id_Joueur = t_joueur_personne.fk_Id_Joueur
                                                            LEFT JOIN t_personne ON t_personne.id_personne = t_joueur_personne.fk_Id_personne 
                                                            GROUP BY Id_Joueur"""
                if id_joueure_sel == 0:
                    # le paramètre 0 permet d'afficher tous les joueures
                    # Sinon le paramètre représente la valeur de l'id du joueure
                    mc_afficher.execute(strsql_personnes_joueures_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du joueure sélectionné avec un nom de variable
                    valeur_id_joueure_selected_dictionnaire = {"value_id_joueure_selected": id_joueure_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_personnes_joueures_afficher_data += """ HAVING Id_Joueur= %(value_id_joueure_selected)s"""

                    mc_afficher.execute(strsql_personnes_joueures_afficher_data, valeur_id_joueure_selected_dictionnaire)

                # Récupère les données de la requête.
                data_personnes_joueures_afficher = mc_afficher.fetchall()
                print("data_personnes ", data_personnes_joueures_afficher, " Type : ", type(data_personnes_joueures_afficher))

                # Différencier les messages.
                if not data_personnes_joueures_afficher and id_joueure_sel == 0:
                    flash("""La table "t_carte_joueur" est vide. !""", "warning")
                elif not data_personnes_joueures_afficher and id_joueure_sel > 0:
                    # Si l'utilisateur change l'Id_Joueur dans l'URL et qu'il ne correspond à aucun joueure
                    flash(f"Le joueure {id_joueure_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données joueures et personnes affichés !!", "success")

        except Exception as Exception_joueures_personnes_afficher:
            code, msg = Exception_joueures_personnes_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception joueures_personnes_afficher : {sys.exc_info()[0]} "
                  f"{Exception_joueures_personnes_afficher.args[0]} , "
                  f"{Exception_joueures_personnes_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("joueures_personnes/joueures_personnes_afficher.html", data=data_personnes_joueures_afficher)


"""
    nom: edit_personne_joueure_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les personnes du joueure sélectionné par le bouton "MODIFIER" de "joueures_personnes_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les personnes contenus dans la "t_personne".
    2) Les personnes attribués au joueure selectionné.
    3) Les personnes non-attribués au joueure sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_personne_joueure_selected", methods=['GET', 'POST'])
def edit_personne_joueure_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_personnes_afficher = """SELECT id_personne, Nom_personne FROM t_personne ORDER BY id_personne ASC"""
                mc_afficher.execute(strsql_personnes_afficher)
            data_personnes_all = mc_afficher.fetchall()
            print("dans edit_personne_joueure_selected ---> data_personnes_all", data_personnes_all)

            # Récupère la valeur de "Id_Joueur" du formulaire html "joueures_personnes_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "Id_Joueur"
            # grâce à la variable "id_joueur_personnes_edit_html" dans le fichier "joueures_personnes_afficher.html"
            # href="{{ url_for('edit_personne_joueure_selected', id_joueur_personnes_edit_html=row.Id_Joueur) }}"
            id_joueur_personnes_edit = request.values['id_joueur_personnes_edit_html']

            # Mémorise l'id du joueure dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_joueur_personnes_edit'] = id_joueur_personnes_edit

            # Constitution d'un dictionnaire pour associer l'id du joueure sélectionné avec un nom de variable
            valeur_id_joueure_selected_dictionnaire = {"value_id_joueure_selected": id_joueur_personnes_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction personnes_joueures_afficher_data
            # 1) Sélection du joueure choisi
            # 2) Sélection des personnes "déjà" attribués pour le joueure.
            # 3) Sélection des personnes "pas encore" attribués pour le joueure choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "personnes_joueures_afficher_data"
            data_personne_joueure_selected, data_personnes_joueures_non_attribues, data_personnes_joueures_attribues = \
                personnes_joueures_afficher_data(valeur_id_joueure_selected_dictionnaire)

            print(data_personne_joueure_selected)
            lst_data_joueure_selected = [item['Id_Joueur'] for item in data_personne_joueure_selected]
            print("lst_data_joueure_selected  ", lst_data_joueure_selected,
                  type(lst_data_joueure_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les personnes qui ne sont pas encore sélectionnés.
            lst_data_personnes_joueures_non_attribues = [item['id_personne'] for item in data_personnes_joueures_non_attribues]
            session['session_lst_data_personnes_joueures_non_attribues'] = lst_data_personnes_joueures_non_attribues
            print("lst_data_personnes_joueures_non_attribues  ", lst_data_personnes_joueures_non_attribues,
                  type(lst_data_personnes_joueures_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les personnes qui sont déjà sélectionnés.
            lst_data_personnes_joueures_old_attribues = [item['id_personne'] for item in data_personnes_joueures_attribues]
            session['session_lst_data_personnes_joueures_old_attribues'] = lst_data_personnes_joueures_old_attribues
            print("lst_data_personnes_joueures_old_attribues  ", lst_data_personnes_joueures_old_attribues,
                  type(lst_data_personnes_joueures_old_attribues))

            print(" data data_personne_joueure_selected", data_personne_joueure_selected, "type ", type(data_personne_joueure_selected))
            print(" data data_personnes_joueures_non_attribues ", data_personnes_joueures_non_attribues, "type ",
                  type(data_personnes_joueures_non_attribues))
            print(" data_personnes_joueures_attribues ", data_personnes_joueures_attribues, "type ",
                  type(data_personnes_joueures_attribues))

            # Extrait les valeurs contenues dans la table "t_personnes", colonne "Nom_personne"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_personne
            lst_data_personnes_joueures_non_attribues = [item['Nom_personne'] for item in data_personnes_joueures_non_attribues]
            print("lst_all_personnes gf_edit_personne_joueure_selected ", lst_data_personnes_joueures_non_attribues,
                  type(lst_data_personnes_joueures_non_attribues))

        except Exception as Exception_edit_personne_joueure_selected:
            code, msg = Exception_edit_personne_joueure_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_personne_joueure_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_personne_joueure_selected.args[0]} , "
                  f"{Exception_edit_personne_joueure_selected}", "danger")

    return render_template("joueures_personnes/joueures_personnes_modifier_tags_dropbox.html",
                           data_personnes=data_personnes_all,
                           data_joueure_selected=data_personne_joueure_selected,
                           data_personnes_attribues=data_personnes_joueures_attribues,
                           data_joueures_non_attribues=data_personnes_joueures_non_attribues)


"""
    nom: update_personne_joueure_selected

    Récupère la liste de tous les personnes du joueure sélectionné par le bouton "MODIFIER" de "joueures_personnes_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les personnes contenus dans la "t_personne".
    2) Les personnes attribués au joueure selectionné.
    3) Les personnes non-attribués au joueure sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_personne_joueure_selected", methods=['GET', 'POST'])
def update_personne_joueure_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du joueure sélectionné
            id_joueure_selected = session['session_id_joueur_personnes_edit']
            print("session['session_id_joueur_personnes_edit'] ", session['session_id_joueur_personnes_edit'])

            # Récupère la liste des personnes qui ne sont pas associés au joueure sélectionné.
            old_lst_data_personnes_joueures_non_attribues = session['session_lst_data_personnes_joueures_non_attribues']
            print("old_lst_data_personnes_joueures_non_attribues ", old_lst_data_personnes_joueures_non_attribues)

            # Récupère la liste des personnes qui sont associés au joueure sélectionné.
            old_lst_data_personnes_joueures_attribues = session['session_lst_data_personnes_joueures_old_attribues']
            print("old_lst_data_personnes_joueures_old_attribues ", old_lst_data_personnes_joueures_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme personnes dans le composant "tags-selector-tagselect"
            # dans le fichier "personnes_joueures_modifier_tags_dropbox.html"
            new_lst_str_personnes_joueures = request.form.getlist('name_select_tags')
            print("new_lst_str_personnes_joueures ", new_lst_str_personnes_joueures)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_personne_joueure_old = list(map(int, new_lst_str_personnes_joueures))
            print("new_lst_personne_joueure ", new_lst_int_personne_joueure_old, "type new_lst_personne_joueure ",
                  type(new_lst_int_personne_joueure_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_personne" qui doivent être effacés de la table intermédiaire "t_joueur_personne".
            lst_diff_personnes_delete_b = list(
                set(old_lst_data_personnes_joueures_attribues) - set(new_lst_int_personne_joueure_old))
            print("lst_diff_personnes_delete_b ", lst_diff_personnes_delete_b)

            # Une liste de "id_personne" qui doivent être ajoutés à la "t_joueur_personne"
            lst_diff_personnes_insert_a = list(
                set(new_lst_int_personne_joueure_old) - set(old_lst_data_personnes_joueures_attribues))
            print("lst_diff_personnes_insert_a ", lst_diff_personnes_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_Id_Joueur"/"Id_Joueur" et "fk_Id_personne "/"id_personne" dans la "t_joueur_personne"
            strsql_insert_personne_joueure = """INSERT INTO t_joueur_personne (Id_Avoir_Joueur, fk_Id_personne , fk_Id_Joueur)
                                                    VALUES (NULL, %(value_fk_personne)s, %(value_fk_joueure)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "Id_Joueur" et "id_personne" dans la "t_joueur_personne"
            strsql_delete_personne_joueure = """DELETE FROM t_joueur_personne WHERE fk_Id_personne  = %(value_fk_personne)s AND fk_Id_Joueur = %(value_fk_joueure)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le joueure sélectionné, parcourir la liste des personnes à INSÉRER dans la "t_joueur_personne".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_personne_ins in lst_diff_personnes_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du joueure sélectionné avec un nom de variable
                    # et "id_personne_ins" (l'id du personne dans la liste) associé à une variable.
                    valeurs_joueure_sel_personne_sel_dictionnaire = {"value_fk_joueure": id_joueure_selected,
                                                               "value_fk_personne": id_personne_ins}

                    mconn_bd.mabd_execute(strsql_insert_personne_joueure, valeurs_joueure_sel_personne_sel_dictionnaire)

                # Pour le joueure sélectionné, parcourir la liste des personnes à EFFACER dans la "t_joueur_personne".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_personne_del in lst_diff_personnes_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du joueure sélectionné avec un nom de variable
                    # et "id_personne_del" (l'id du personne dans la liste) associé à une variable.
                    valeurs_joueure_sel_personne_sel_dictionnaire = {"value_fk_joueure": id_joueure_selected,
                                                               "value_fk_personne": id_personne_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_personne_joueure, valeurs_joueure_sel_personne_sel_dictionnaire)

        except Exception as Exception_update_personne_joueure_selected:
            code, msg = Exception_update_personne_joueure_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_personne_joueure_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_personne_joueure_selected.args[0]} , "
                  f"{Exception_update_personne_joueure_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_joueur_personne",
    # on affiche les joueures et le(urs) personne(s) associé(s).
    return redirect(url_for('joueures_personnes_afficher', id_joueure_sel=id_joueure_selected))


"""
    nom: personnes_joueures_afficher_data

    Récupère la liste de tous les personnes du joueure sélectionné par le bouton "MODIFIER" de "joueures_personnes_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des personnes, ainsi l'utilisateur voit les personnes à disposition

    On signale les erreurs importantes
"""


def personnes_joueures_afficher_data(valeur_id_joueure_selected_dict):
    print("valeur_id_joueure_selected_dict...", valeur_id_joueure_selected_dict)
    try:

        strsql_joueure_selected = """SELECT Id_Joueur, Nom_Joueur, Prenom_Joueur, Nationnaliter_Joueur, Club_Joueur, GROUP_CONCAT(id_personne) as Id_Avoir_Joueur FROM t_joueur_personne
                                        INNER JOIN t_carte_joueur ON t_carte_joueur.Id_Joueur = t_joueur_personne.fk_Id_Joueur
                                        INNER JOIN t_personne ON t_personne.id_personne = t_joueur_personne.fk_Id_personne 
                                        WHERE Id_Joueur = %(value_id_joueure_selected)s"""

        strsql_personnes_joueures_non_attribues = """SELECT id_personne, Nom_personne FROM t_personne WHERE id_personne not in(SELECT id_personne as idPersonnesJoueurs FROM t_joueur_personne
                                                    INNER JOIN t_carte_joueur ON t_carte_joueur.Id_Joueur = t_joueur_personne.fk_Id_Joueur
                                                    INNER JOIN t_personne ON t_personne.id_personne = t_joueur_personne.fk_Id_personne 
                                                    WHERE Id_Joueur = %(value_id_joueure_selected)s)"""

        strsql_personnes_joueures_attribues = """SELECT Id_Joueur, id_personne, Nom_personne FROM t_joueur_personne
                                            INNER JOIN t_carte_joueur ON t_carte_joueur.Id_Joueur = t_joueur_personne.fk_Id_Joueur
                                            INNER JOIN t_personne ON t_personne.id_personne = t_joueur_personne.fk_Id_personne 
                                            WHERE Id_Joueur = %(value_id_joueure_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_personnes_joueures_non_attribues, valeur_id_joueure_selected_dict)
            # Récupère les données de la requête.
            data_personnes_joueures_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("personnes_joueures_afficher_data ----> data_personnes_joueures_non_attribues ", data_personnes_joueures_non_attribues,
                  " Type : ",
                  type(data_personnes_joueures_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_joueure_selected, valeur_id_joueure_selected_dict)
            # Récupère les données de la requête.
            data_joueure_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_joueure_selected  ", data_joueure_selected, " Type : ", type(data_joueure_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_personnes_joueures_attribues, valeur_id_joueure_selected_dict)
            # Récupère les données de la requête.
            data_personnes_joueures_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_personnes_joueures_attribues ", data_personnes_joueures_attribues, " Type : ",
                  type(data_personnes_joueures_attribues))

            # Retourne les données des "SELECT"
            return data_joueure_selected, data_personnes_joueures_non_attribues, data_personnes_joueures_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans personnes_joueures_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans personnes_joueures_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_personnes_joueures_afficher_data:
        code, msg = IntegrityError_personnes_joueures_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans personnes_joueures_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_personnes_joueures_afficher_data.args[0]} , "
              f"{IntegrityError_personnes_joueures_afficher_data}", "danger")
