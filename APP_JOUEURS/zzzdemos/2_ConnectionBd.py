"""
    Fichier : 2_Connectionbd.py
    Auteur : OM 2021.03.03 Connection par l'instanciation de la classe Toolsbd.

    On obtient un objet "objet_connectbd"
    Cela permet de se connecter à la base de donnée.
"""

from APP_JOUEURS.database.database_tools import Toolsbd

try:

    """
        Une connection à la BD simplement avec l'instanciation de la "CLASSE" Toolsbd()
        Un curseur va être nécessaire pour se déplacer dans la BD.
    """
    objet_connectbd = Toolsbd()
    connect_mabd = objet_connectbd.connect_database()
    curseur_mabd = connect_mabd.cursor()

    """
        Une seule requête pour montrer la récupération des données dans la BD en MySql.
        Il n'y a aucun contrôle, aucun test, sans traitements d'erreurs.
        Tous ces tests restent à découvrir dans le sujet suivant.
    """
    strsql_joueures_afficher = """SELECT id_Joueur, Prenom_Joueur FROM t_carte_joueur ORDER BY id_Joueur ASC"""
    curseur_mabd.execute(strsql_joueures_afficher)
    data_joueures = curseur_mabd.fetchall()

    print("data_joueures ", data_joueures, " Type : ", type(data_joueures))

    curseur_mabd.close()
    connect_mabd.close()

except Exception as ErreurConnectionBD:
    print(f"Connection à la BD Impossible !"
          f"{ErreurConnectionBD.args[0]} , "
          f"{ErreurConnectionBD}")



